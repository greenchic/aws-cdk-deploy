FROM alpine:3.11

RUN apk add --update --no-cache bash python python-dev py-pip build-base jq nodejs npm

RUN pip install awscli --upgrade --user

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh

RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
