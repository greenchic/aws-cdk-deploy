# Bitbucket Pipelines Pipe: AWS CDK deploy

Deploy infrastructure as code using AWS CDK

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: sightsoundtheatres/aws-cdk-deploy:0.2.6
    variables:
      AWS_ACCESS_KEY_ID: "<string>"
      AWS_SECRET_ACCESS_KEY: "<string>"
      AWS_DEFAULT_REGION: "<string>"
      STACK_NAME: "<string>"
      # AWS_ROLE_ARN: "<string>" # Optional
      # DIRECTORY_NAME: "<string>" # Optional
      # NPM_TOKEN: "<string>" # Optional
      # AWS_ROLE_ARN_CFN: "<string>" # Optional
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable                  | Usage                                                       |
| ------------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (*)     | AWS access key. |
| AWS_SECRET_ACCESS_KEY (*) | AWS secret key. |
| AWS_DEFAULT_REGION (*)    | The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference_. |
| STACK_NAME (*)            | The name of the AWS CloudFormation stack. If the stack does not exist, it will be created. |
| AWS_ROLE_ARN              | The ARN of the AWS role for the CLI to assume when deploying the stack. |
| DIRECTORY_NAME            | The name of the directory to run CDK commands inside of. |
| NPM_TOKEN                 | The token to use for private NPM dependencies. |
| AWS_ROLE_ARN_CFN          | The ARN of the AWS role for CloudFormation to assume when deploying the stack. |
| DEBUG                     | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites
* An IAM user is configured with sufficient permissions to perform a deployment of your application using CloudFormation.

## Examples

Basic example:

```yaml
script:
  - pipe: sightsoundtheatres/aws-cdk-deploy:0.2.6
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: "us-east-1"
      STACK_NAME: "my-stack-name"
```

Advanced example:

```yaml
script:
  - pipe: sightsoundtheatres/aws-cdk-deploy:0.2.6
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: "us-east-1"
      STACK_NAME: "my-stack-name"
      AWS_ROLE_ARN: arn:aws:iam::ACCOUNT_ID:role/ROLE_NAME
      NPM_TOKEN: $NPM_TOKEN
      AWS_ROLE_ARN_CFN: arn:aws:iam::ACCOUNT_ID:role/ROLE_NAME
      DEBUG: "true"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by DeveloperTeam@sight-sound.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
