# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.6

- patch: Remove cdk synth

## 0.2.5

- patch: Add support for cdk bootstrap

## 0.2.4

- patch: Add cloudformation role support

## 0.2.3

- patch: Add support for NPM tokens and private NPM registry dependencies

## 0.2.2

- patch: Fix approval pt. 2

## 0.2.1

- patch: Fix approval

## 0.2.0

- minor: Do not require approval of deploy

## 0.1.5

- patch: Fix cd command

## 0.1.4

- patch: Add DIRECTORY_NAME variable

## 0.1.3

- patch: Fixed AWS_ROLE_ARN option again 1 more time

## 0.1.2

- patch: Fixed AWS_ROLE_ARN option again

## 0.1.1

- patch: Fixed AWS_ROLE_ARN option

## 0.1.0

- minor: Initial release
- patch: Initial code
