#!/usr/bin/env bash
#
# Deploy infrastructure as code using AWS CDK
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
STACK_NAME=${STACK_NAME:?'STACK_NAME variable missing.'}

# Default parameters
AWS_ROLE_ARN=${AWS_ROLE_ARN:=""}
AWS_ROLE_ARN_CFN=${AWS_ROLE_ARN_CFN:=""}
DIRECTORY_NAME=${DIRECTORY_NAME:=""}
NPM_TOKEN=${NPM_TOKEN:=""}
DEBUG=${DEBUG:="false"}

if [[ "${AWS_ROLE_ARN}" != "" ]]; then
  info "Attempting to assume role ${AWS_ROLE_ARN}"
  temp_role=$(~/.local/bin/aws sts assume-role --role-arn ${AWS_ROLE_ARN} --role-session-name Bitbucket)
  export AWS_ACCESS_KEY_ID=$(echo ${temp_role} | jq -r .Credentials.AccessKeyId)
  export AWS_SECRET_ACCESS_KEY=$(echo ${temp_role} | jq -r .Credentials.SecretAccessKey)
  export AWS_SESSION_TOKEN=$(echo ${temp_role} | jq -r .Credentials.SessionToken)
  info "Successfully assumed role ${AWS_ROLE_ARN}"
fi

# Get AWS account ID for later use
export AWS_ACCOUNT_ID=$(~/.local/bin/aws sts get-caller-identity | jq -r .Account)

if [[ "${DIRECTORY_NAME}" != "" ]]; then
  info "Changing directory to ${DIRECTORY_NAME}"
  cd ${DIRECTORY_NAME}
fi

if [[ "${NPM_TOKEN}" != "" ]]; then
  info "Setting up .npmrc file with token"
  echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > .npmrc
fi

info "Attempting to install NPM dependencies"
run npm ci
info "Successfully installed NPM dependencies"

info "Attempting to bootstrap CDK toolkit"
run npm run cdk -- bootstrap aws://${AWS_ACCOUNT_ID}/${AWS_DEFAULT_REGION}
info "Successfully bootstrapped CDK toolkit"

info "Attempting to deploy CDK stack"
if [[ "${AWS_ROLE_ARN_CFN}" != "" ]]; then
  run npm run cdk -- deploy ${STACK_NAME} --require-approval never --role-arn ${AWS_ROLE_ARN_CFN}
else
  run npm run cdk -- deploy ${STACK_NAME} --require-approval never
fi

if [[ "${status}" == "0" ]]; then
  success "Successfully deployed CDK stack!"
else
  fail "Error!"
fi
